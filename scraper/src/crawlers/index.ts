import crawlCitiesSources from "./crawlCitiesSources";
import crawlDistrictsSources from "./crawlDistrictsSources";
import crawlAdsSources from "./crawlAdsSources";
import { cityType } from "../types";

const crawlSources = async (): Promise<{ cities: cityType[] }> => {
  try {
    return {
      cities: await crawlAdsSources(
        await crawlDistrictsSources(await crawlCitiesSources())
      )
    };
  } catch (e) {
    console.log(`Failed to get links. ${e}`);
    console.log("Retrying...");

    return await crawlSources();
  }
};

export default crawlSources;
