import { fetchCitiesHTMLPage } from "../api";
import { scrapeCitiesData } from "../scrapers";
import { cityType } from "../types";

const crawlCitiesSources = async (): Promise<cityType[]> => {
  try {
    console.log("Starting to get cities data.");

    const citiesPageHTML = await fetchCitiesHTMLPage();

    return scrapeCitiesData(citiesPageHTML);
  } catch (e) {
    throw new Error(`Failed to get cities data. Message: ${e}`);
  }
};

export default crawlCitiesSources;
