import { fetchDistrictsHTMLPage } from "../api";
import { scrapeDistrictsData } from "../scrapers";
import { cityType } from "../types";

const getDistrictsData = async (city: cityType) => {
  try {
    console.log(`Starting to get ${city.city} city districts`);

    const districtsHTMLPage = await fetchDistrictsHTMLPage(city);

    return scrapeDistrictsData(districtsHTMLPage, city);
  } catch (e) {
    throw new Error(`Failed to get ${city.city} city districts. Message: ${e}`);
  }
};

const crawlCitiesSources = async (cities: cityType[]): Promise<cityType[]> => {
  try {
    console.log("Starting to get districts data");

    const citiesCopy = [...cities];

    for (let i = 0; i <= cities.length - 1; i++) {
      citiesCopy[i] = {
        ...citiesCopy[i],
        districts: await getDistrictsData(citiesCopy[i])
      };
    }

    return citiesCopy;
  } catch (e) {
    throw new Error(`Failed to get districts data. Message: ${e}`);
  }
};

export default crawlCitiesSources;
