import { fetchDistrictHTMLPage, fetchAdsHTMLPage } from "../api";
import { scrapeLastAdsPageNumber, scrapePageAdsHRefs } from "../scrapers";
import {
  destructAdId,
  generateAdsPagesHRefs,
  flattenPageAdsHrefs
} from "../utils";
import cityType from "../types/cityType";
import districtType from "../types/districtType";
import adType from "../types/adType";

const getLastAdsPageNumber = async (
  district: districtType
): Promise<number> => {
  try {
    console.log(
      `Starting to get ${district.district} district last ads page number`
    );

    const districtHTMLPage = await fetchDistrictHTMLPage(district);

    return scrapeLastAdsPageNumber(districtHTMLPage);
  } catch (e) {
    throw new Error(
      `Failed to get ${district.district} district last ads page number. Message: ${e}`
    );
  }
};

const getPageAdsHRefs = async (pageHRef: string): Promise<string[]> => {
  try {
    console.log(`Starting to get ads hrefs from ${pageHRef} page`);

    const adsPageHTML = await fetchAdsHTMLPage(pageHRef);

    return scrapePageAdsHRefs(adsPageHTML, pageHRef);
  } catch (e) {
    throw new Error(
      `Failed to get ads hrefs from ${pageHRef} page. Message: ${e}`
    );
  }
};

const getAdsHRefs = async (pagesHRefs: string[]): Promise<string[]> => {
  try {
    console.log(`Starting to get district ads hrefs`);

    let pageAdsHRefs = [];

    for (let i = 0; i <= pagesHRefs.length - 1; i++) {
      pageAdsHRefs[i] = await getPageAdsHRefs(pagesHRefs[i]);
    }

    return flattenPageAdsHrefs(pageAdsHRefs);
  } catch (e) {
    throw new Error(`Failed to get district ads hrefs. Message: ${e}`);
  }
};

const getAds = async (district: districtType): Promise<adType[]> => {
  try {
    console.log(`Starting to get ${district.district} district ads`);

    const lastAdsPageNumber = await getLastAdsPageNumber(district);
    const adsPagesHRefs = generateAdsPagesHRefs(lastAdsPageNumber, district);
    const adsHrefs = await getAdsHRefs(adsPagesHRefs);

    return adsHrefs.map(adHRef => ({
      ad: destructAdId(adHRef),
      href: adHRef
    }));
  } catch (e) {
    throw new Error(
      `Failed to get ${district.district} district ads. Message: ${e}`
    );
  }
};

const getDistrictsData = async (city: cityType): Promise<districtType[]> => {
  try {
    console.log(`Starting to get ${city.city} city districts`);

    const districtsCopy = [...city.districts];

    for (let i = 0; i <= districtsCopy.length - 1; i++) {
      districtsCopy[i] = {
        ...districtsCopy[i],
        ads: await getAds(districtsCopy[i])
      };
    }

    return districtsCopy;
  } catch (e) {
    throw new Error(`Failed to get ${city.city} city districts. Message: ${e}`);
  }
};

// TODO: Reuse
const crawlAdsSources = async (cities: cityType[]): Promise<cityType[]> => {
  try {
    console.log("Starting to get districts data");

    const citiesCopy = [...cities];

    for (let i = 0; i <= cities.length - 1; i++) {
      citiesCopy[i] = {
        ...citiesCopy[i],
        districts: await getDistrictsData(citiesCopy[i])
      };
    }

    return citiesCopy;
  } catch (e) {
    throw new Error(`Failed to get districts data. Message: ${e}`);
  }
};

export default crawlAdsSources;
