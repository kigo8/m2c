import districtType from "../types/districtType";

// TODO: Channge to recurssion
const generateAdsPagesHRefs = (
  lastAdsPageNumber: number,
  district: districtType
): string[] => {
  let pagesURLs: string[] = [];

  for (let i = 1; i <= lastAdsPageNumber; i++)
    pagesURLs.push(district.href + `page${i}.html`);

  return pagesURLs;
};

export default generateAdsPagesHRefs;
