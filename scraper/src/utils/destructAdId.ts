const destructAdId = (adHRef: string): string => {
  const idRegex = /(\/)((?!.*\1))(.*).html/gm;
  const match = idRegex.exec(adHRef);

  if (!match) throw "Cant find id in ad href";

  return match[3];
};

export default destructAdId;
