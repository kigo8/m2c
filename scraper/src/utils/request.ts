import request, { RequestPromise } from "request-promise";
import ssConfig from "../../config/ss.config";

const headers = {
  "User-Agent":
    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.79 Safari/537.36",
  timeout: 10 * 1000
};

export default (href: string): RequestPromise<string> => {
  return request({
    ...headers,
    uri: ssConfig.baseURL + href
  });
};
