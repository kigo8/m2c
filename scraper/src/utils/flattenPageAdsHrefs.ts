const flattenPageAdsHrefs = (pageAdsHRefs: string[][]): string[] => {
  return pageAdsHRefs.reduce((acc, curr) => {
    acc.push(...curr);

    return acc;
  }, []);
};

export default flattenPageAdsHrefs;
