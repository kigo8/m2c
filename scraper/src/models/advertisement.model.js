const mongoose = require("mongoose");

const AdvertisementSchema = new mongoose.Schema({
  apartment: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "apartment",
    required: true
  },
  dealType: {
    type: String,
    required: [true, "is_required"]
  },
  m2Price: {
    type: number,
    required: [true, "is_required"]
  },
  price: {
    type: number,
    required: [true, "is_required"]
  },
  publishmentDate: {
    type: String,
    required: [true, "is_required"]
  },
  views: {
    type: Number,
    required: [true, "is_required"]
  },
  href: {
    type: String,
    required: [true, "is_required"]
  }
});

const AdvertisementModel = mongoose.model("advertisement", AdvertisementSchema);

module.exports = AdvertisementModel;
