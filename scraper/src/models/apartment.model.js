const mongoose = require("mongoose");

const ApartmentSchema = new mongoose.Schema({
  city: {
    type: String,
    required: [true, "is_required"]
  },
  district: {
    type: String,
    required: [true, "is_required"]
  },
  street: {
    type: String,
    required: [true, "is_required"]
  },
  rooms: {
    type: Number,
    required: [true, "is_required"]
  },
  size: {
    type: Number,
    required: [true, "is_required"]
  },
  floor: {
    current: {
      type: Number,
      required: [true, "is_required"]
    },
    max: {
      type: Number,
      required: [true, "is_required"]
    }
  },
  series: {
    type: String,
    required: [true, "is_required"]
  },
  type: {
    type: String,
    required: [true, "is_required"]
  },
  facilities: {
    type: String,
    required: [true, "is_required"]
  }
});

const ApartmentModel = mongoose.model("apartment", ApartmentSchema);

module.exports = ApartmentModel;
