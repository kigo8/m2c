import districtType from "./districtType";

interface cityType {
  city: string;
  href: string;
  districts: districtType[];
}

export default cityType;
