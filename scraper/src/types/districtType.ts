import adType from "./adType";

interface districtType {
  district: string;
  href: string;
  ads: adType[];
}

export default districtType;
