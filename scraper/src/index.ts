import crawlSources from "./crawlers";

// const connectDB = require("./connectDB");

// connectDB();

const main = async () => {
  const sources = await crawlSources();

  console.log(sources);
};

main();
