import cheerio from "cheerio";
import cityType from "../types/cityType";

const scrapeCitiesData = (citiesHTML: string): cityType[] => {
  console.log("Starting to scrape cities data.");

  try {
    const $ = cheerio.load(citiesHTML);

    const cities: cityType[] = $(".category > a")
      .map((_, el) => {
        return {
          city: $(el).text(),
          href: $(el).attr("href")
        };
      })
      .get()
      .filter(
        ({ city }) => city !== "Cits" && city !== "Dzīvokļi ārpus Latvijas" // Only accross existing cities and inside country
      )
      .sort((a, b) => a.city.localeCompare(b.city)); // Sort alphabetically

    return cities;
  } catch (e) {
    throw new Error(`Failed to scrape cities data. Message: ${e}`);
  }
};

export default scrapeCitiesData;
