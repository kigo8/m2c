import cheerio from "cheerio";

const scrapePageAdsHRefs = (pageHTML: string, pageHRef: string): string[] => {
  try {
    console.log(`Starting to scrape ads hrefs from ${pageHRef}`);

    const $ = cheerio.load(pageHTML);
    const trElements = $("#filter_frm")
      .children("table")
      .eq(1)
      .find("tr");

    return $(trElements)
      .filter(index => {
        // First element is header and last one is empty
        return index > 0 && index < $(trElements).length - 1;
      })
      .map((_, trEl) => {
        const adHRef = $(trEl)
          .find(".msga2 > a")
          .attr("href");

        return adHRef;
      })
      .get();
  } catch (e) {
    throw new Error(
      `Failed to scrape ads hrefs from ${pageHRef}. Message: ${e}`
    );
  }
};

export default scrapePageAdsHRefs;
