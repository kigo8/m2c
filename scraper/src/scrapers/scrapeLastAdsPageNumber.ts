import cheerio from "cheerio";

const parsePageNumber = (pageLink: string): string =>
  pageLink.replace(/\D+/g, "");

const scrapeLastAdsPageNumber = (districtHTML: string): number => {
  try {
    const $ = cheerio.load(districtHTML);
    // First page button in the list redirect to last page
    const lastPageHRef = $("a[name='nav_id']:first-child").attr("href");

    if (lastPageHRef) return +parsePageNumber(lastPageHRef);

    return 1;
  } catch (e) {
    throw new Error(`Failed to parse district HTML. ${e}`);
  }
};

export default scrapeLastAdsPageNumber;
