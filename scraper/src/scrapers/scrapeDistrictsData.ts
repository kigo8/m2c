import cheerio from "cheerio";
import cityType from "../types/cityType";
import districtType from "../types/districtType";

const scrapeDistrictsData = (
  districtsHTML: string,
  city: cityType
): districtType[] => {
  console.log(`Starting to scrape ${city.city} city districts data`);

  try {
    const $ = cheerio.load(districtsHTML);

    return $(".category > a")
      .map((_, el) => {
        return {
          district: $(el).text(),
          href: $(el).attr("href")
        };
      })
      .get()
      .filter(
        ({ district }) => district !== "Cits" && district !== "Visi sludinājumi"
      )
      .sort((a, b) => a.district.localeCompare(b.district)); // Sort alphabetically
  } catch (e) {
    throw new Error(
      `Failed to scrape ${city.city} city districts data. Message: ${e}`
    );
  }
};

export default scrapeDistrictsData;
