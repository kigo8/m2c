export { default as fetchCitiesHTMLPage } from "./fetchCitiesHTMLPage";
export { default as fetchDistrictsHTMLPage } from "./fetchDistrictsHTMLPage";
export { default as fetchDistrictHTMLPage } from "./fetchDistrictHTMLPage";
export { default as fetchAdsHTMLPage } from "./fetchAdsHTMLPage";
