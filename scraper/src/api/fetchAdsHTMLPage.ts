import { request } from "../utils";

const fetchAdsHTMLPage = async (pageHRef: string): Promise<string> => {
  try {
    console.log(`Starting to fetch ${pageHRef} ads page HTML`);

    return await request(pageHRef);
  } catch (e) {
    throw new Error(`Failed to fetch ${pageHRef} ads page HTML. ${e}`);
  }
};

export default fetchAdsHTMLPage;
