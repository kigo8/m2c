import { request } from "../utils";
import cityType from "../types/cityType";

const fetchDistrictsHTMLPage = async (city: cityType): Promise<string> => {
  try {
    console.log(`Starting to fetch ${city.city} city districts HTML page`);

    return request(city.href);
  } catch (e) {
    throw new Error(
      `Failed to fetch ${city.city} city districts HTML page. Message: ${e}`
    );
  }
};

export default fetchDistrictsHTMLPage;
