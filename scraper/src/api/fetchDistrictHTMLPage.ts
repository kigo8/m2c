import { request } from "../utils";
import districtType from "../types/districtType";

const fetchDistrictHTMLPage = async (
  district: districtType
): Promise<string> => {
  try {
    console.log(`Starting to fetch ${district.href} district HTML page.`);

    return await request(district.href);
  } catch (e) {
    throw new Error(
      `Failed to fetch ${district.href} district HTML page. ${e}`
    );
  }
};

export default fetchDistrictHTMLPage;
