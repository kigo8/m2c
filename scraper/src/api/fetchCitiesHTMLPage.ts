import { request } from "../utils";
import ssConfig from "../../config/ss.config";

const fetchCitiesHTMLPage = async (): Promise<string> => {
  try {
    console.log("Starting to fetch cities HTML page.");

    return await request(ssConfig.citiesHRef);
  } catch (e) {
    throw new Error(`Failed to fetch cities HTML page. Message: ${e}`);
  }
};

export default fetchCitiesHTMLPage;
