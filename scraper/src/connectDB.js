const mongoose = require("mongoose");
const DB_CONFIG = require("../config/db.config");

const connectDB = async () =>
  await mongoose
    .connect(DB_CONFIG.ADDRESS, DB_CONFIG.OPTIONS)
    .then(() =>
      console.log(`Scraper DB is connected on port ${DB_CONFIG.PORT}`)
    )
    .catch(err =>
      console.log(`Error! Scraper DB is failed to connect. Message: ${err}`)
    );

module.exports = connectDB;
