const SERVICE_NAME = "mongo";
const PORT = 27017;
const NAME = "scraper";

export default {
  ADDRESS: `mongodb://${SERVICE_NAME}:${PORT}/${NAME}`,
  OPTIONS: {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true
  },
  PORT
};
